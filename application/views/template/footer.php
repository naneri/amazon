    </div>
    <div class="footer">
        <div class="container">
            <div class="col-md-4">

            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 text-right">
                <p><i class="fa fa-envelope fa-fw"></i> contact@zabor.kg</p>
            </div>
        </div>
    </div>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-50333385-1', 'zabor.kg');
        ga('send', 'pageview');

    </script>
</body>