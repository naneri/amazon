<!doctype HTML>
<html>
<head>
    <meta charset="utf-8">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.1.1/readable/bootstrap.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <title>
        <?php if(isset($title)):echo $title?>
        <?php else: echo 'Amazon - Kyrgyzstan'; ?>
        <?php endif;?>
    </title>
    
    <link rel="shortcut icon" href="/images/favicon.ico" />
    
   
    
    <style>
        .footer{
            background-image: url(<?php echo base_url('images/3.png')?>);
            color:white;
            padding-top:20px;
            padding-bottom:20px;
            margin-top:40px;
        }
        .mainimage{
            max-width: 100%;
            height: 100px;
        }
        .breadcrumb{
            margin-top: 20px;
        }
        
        #content{
            min-height:610px;
        }
        
        .mincontainer{
            min-height:610px;
        }
    </style>
    
</head>
<body>

    <div style="margin-bottom:0px" class="navbar navbar-default navbar-static ">
        <div class="container">
            <div class="navbar-header">
                
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                </button>
            </div>								
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo site_url('');?>">Главная</a></li>
   <!--                 <li><a href="<?php echo site_url('item/sample');?>">Посмотреть</a></li> -->
                    <li><a href="<?php echo site_url('item/about');?>">О нас</a></li>
                    <li><a href="http://old.zabor.kg" target="_blank">Zabor.kg</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container mincontainer">
        <?php if($this->session->flashdata('item')): ?>
        <div class="flashdata alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('item'); ?>
        </div>
        <?php endif; ?>