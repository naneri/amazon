    
<div class="row">
    <div class="col-md-9">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>">Amazon</a></li>
          <li><a href='<?php echo base_url('item/watch/' . $item['id']); ?>'><?php echo substr($item['title'],0 ,30) . '...'; ?></a></li>
          <li class="active">Доставка - <?php echo $shipper['name']; ?></li>
        </ol>
    </div>   
</div>

     <div class="row">   
        <div class="col-md-9">
            <p>Чтобы заказать товар самостоятельно:</p>
            <p>1. Пройдите по <strong><a target="_blank" href="<?php echo $item['url']; ?>">этой</a></strong> ссылке и убедитесь что цена товара соответствует <?php echo $item['price']; ?>$ <span class="small text-muted">(иначе вам придётся доплачивать разницу, если товар стал дороже, или наоборот, вам надо будет платить меньше, если товар стал дешевле). Также вы можете уведомить нас об изменении цены нажав <a href="<?php echo base_url('item/priceupdate/'.$item['id']);?>">сюда</a></span></p>            
            <p>2. Ознакомьтесь с <b><a target="_blank" href='<?php echo $shipper["forwarding"]; ?>'>инструкцией посредника</a></b> по заказу товара.</p>
            <p>3. Ознакомьтесь с особенностями заказа товаров с Amazon.com <b><a target="_blank" href="http://secondbid.ru/buy-amazon">здесь</a></b></p>
            <p>4. Закажите и оформите товар согласно вышеуказанным инструкциям.</p>
            
            <hr class="featurette-divider">
            <p class="text-muted">Данные о весе и габаритах упаковки предоставлены сайтом <a href="http://amazon.com">Amazon.com</a>. Стоимость доставки расчитана исходя из тарифов компании <a href="<?php echo $shipper['url']; ?>"><?php echo $shipper['name']; ?></a>. Вы можете ознакомится с отзывами о компании <a href="<?php echo $shipper['url']; ?>"><?php echo $shipper['name']; ?></a> и тарифами на <a href="<?php echo $shipper['diesel_topic']; ?>">страничке в дизеле</a>. </p>
            <p class="text-muted">В будущем мы планируем добавить инструкции для заказа с расчётом стоимости через других посредников: <a href="http://hotkg.com/">Hot.kg</a>,  и т.д.</p>
        </div>    
    </div>
