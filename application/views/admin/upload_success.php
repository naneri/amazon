<?php if($status == 'success'): ?>
<div class="container">
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Объявление добавлено.
    </div>
</div>
<?php endif; ?>