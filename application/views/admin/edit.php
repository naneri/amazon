<div class="row">
        <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url('admin/index');?>">Admin</a></li>
              <li class="active"><?php echo substr($item['title'],0 ,30) . '...'; ?></li>
            </ol>
        </div>    
    </div>


<div class="row">
    
    <div class="col-md-4">
            <img class="img-responsive" src="<?php echo $item['item_image']; ?>"> </br>
    </div>
    
    <div class="col-md-5">
        <b>Название:</b><?php echo $item['title']; ?> </br>
        <a target="_blank" href="<?php echo $item['url']; ?>">Описание (на английском)</a> </br>
        <hr class="featurette-divider">
        <?php echo form_open_multipart('admin/update');?>
        <input type="hidden" name="id" value="<?php echo $item['id'];?>">
        <p><b>Price:</b></p>
        <input class="form-control" name="price" type="text" value="<?php echo $item['price']; ?>">
        <hr class="featurette-divider">
        <p><b>Description:</b></p>
        <textarea class="form-control" name="description" type="textarea" rows="3"><?php echo $item['description']; ?></textarea>
        <hr class="featurette-divider">
        <button type="submit" class="pull-right btn btn-info">Edit</button>
        
        </form>
    </div>
</div>
    
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.min.js');?>"></script>
<script>
        tinymce.init({
            selector:'textarea',
            fullpage_default_encoding: "UTF-8"
        });
</script>