<!doctype HTML>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .mainimage{
            max-width: 100%;
            height: 100px;
        }
        .breadcrumb{
            margin-top: 20px;
        }
        .maincontainer{
            padding-top: 20px;
        }
    </style>
    
</head>
<body>

    <div style="margin-bottom:0px" class="navbar navbar-default navbar-static ">
        <div class="navbar-header">
            <a href="<?php echo site_url('');?>" class="navbar-brand"><strong>Amazon</strong></a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
            </button>
        </div>								
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo site_url('admin/index');?>">Главная</a></li>
                <li><a href="<?php echo site_url('admin/add');?>">Добавить</a></li>
                <li><a href="<?php echo site_url('auth/logout');?>">Выйти</a></li>
            </ul>
        </div>
    </div>
    
    <div class='container maincontainer'>
        <?php if($this->session->flashdata('item')): ?>
        <div class="flashdata alert alert-info alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('item'); ?>
        </div>
        <?php endif; ?>