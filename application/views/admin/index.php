
    <div class="col-md-12">
        <p class="text-center">Страницы:<?php echo $links; ?></p>
        
            
        <table class='table'>
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Watch</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($items as $item): ?>
                <tr>
                    <td><img class="img-responsive mainimage" src="<?php echo $item['item_image']; ?>"></td>
                    <td><?php echo substr($item['title'],0 ,30).'...'; ?></td>
                    <td>
                        <?php if($item['description']):?>
                            <i style="color:green" class="fa fa-check fa-fw"></i>
                        <?php else: ?>
                            <i style="color:red" class="fa fa-times fa-fw"></i>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if($item['updated'] == 1):?>
                            <strong><span style="color:red">Needs update</span></strong>
                        <?php else: ?>
                            <span>Ok</span>
                        <?php endif; ?>
                    </td>
                    <td><a href='<?php echo base_url('item/watch/'.$item['id']); ?>'>Подробнее</a></td>
                    <td><a href='<?php echo base_url('admin/edit/'.$item['id']); ?>'>Редактировать</a></td>
                    <td><a href='<?php echo base_url('admin/delete/'.$item['id']); ?>'>Удалить</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>     
    </div>

