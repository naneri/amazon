
    <div class="row">
        <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url();?>">Amazon</a></li>
              <li class="active"><?php echo substr($item['title'],0 ,30) . '...'; ?></li>
            </ol>
        </div>    
    </div>


    <div class="row">
        <div class="col-md-4">
            <img class="img-responsive" src="<?php echo $item['item_image']; ?>"> </br>  
        </div>
        <div class="col-md-5">
            <b>Название:</b><?php echo $item['title']; ?> </br>
            <a target="_blank" href="<?php echo $item['url']; ?>">Описание (на английском)</a> </br></br>
            <p class="text-center"><b>Краткое описание:</b></p>
            <div class="description">
                <?php echo $item['description'];?>
            </div>
            <hr class="featurette-divider">
            <p class="text-center"><b>Цена на Amazon.com:</b></p>
            Цена со скидкой:<?php echo $item['price']; ?>$ </br></br>
            
            <p class="text-center"><b>Заказать с карточки посредника:</b></p>
            Gidex: 
                <?php echo item_shipping_gidex_card($item); ?>$
                <a class="text-muted" href="<?php echo base_url(('shipper/order_card/Gidex').'/'.$item['id']);?>">(Как заказать)</a> 
                </br>
            ELS: 
                <?php echo item_shipping_els_card($item); ?>$ 
                <a class="text-muted" href="<?php echo base_url(('shipper/order_card/ELS').'/'.$item['id']);?>">(Как заказать)</a> 
                </br></br></br> 
             
            <p class="text-center"><b>Заказать со своей карточки:</b></p>
            Gidex: 
                 <?php echo item_shipping_gidex_self($item); ?>$
                 <a class="text-muted" href="<?php echo base_url(('shipper/order_self/Gidex').'/'.$item['id']);?>">(Как заказать)</a>
                 </br>
            ELS:
                 <?php echo item_shipping_els_self($item); ?>$ 
                 <a class="text-muted" href="<?php echo base_url(('shipper/order_self/ELS').'/'.$item['id']);?>">(Как заказать)</a>
                 </br>
           
            
           <hr class="featurette-divider">
           
           <p class="text-center"><b>Данные об упаковке:</b></p>
           <b>Высота:</b> <?php echo $item['height']; ?> см</br>
           <b>Длина:</b> <?php echo $item['length']; ?> см</br>
           <b>Ширина:</b> <?php echo $item['width']; ?> см</br></br>
           <b>Вес:</b> <?php echo number_format($item['weight']*0.00453,2); ?> кг</br>
           <hr class="featurette-divider">
           
           <p class="text-center"><b>Поделиться:</b></p>
           <a href='http://www.facebook.com/sharer.php?u=<?php echo current_url() ; ?>'><img src="<?php echo base_url();?>/images/facebook.png" alt="Share on Facebook" title="Share on Facebook" /></a>
           <a href='http://twitter.com/share?url=<?php echo current_url() ; ?>'><img src="<?php echo base_url();?>/images/twitter.png" alt="Share on Facebook" title="Share on Facebook" /></a>
           <a href='https://plus.google.com/share?url=<?php echo current_url() ; ?>'><img src="<?php echo base_url();?>/images/google.png" alt="Share on Facebook" title="Share on Facebook" /></a> 
        </div>
    </div>
