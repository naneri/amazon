
    <div class="col-md-12">
        <p class="text-center">Страницы:<?php echo $links; ?></p>
        
        <?php foreach($items as $item): ?>
        
        <div class="col-md-3 text-center">
            <div class="panel panel-info">
              <div class="panel-heading">
                 <?php echo substr($item['title'],0 ,30).'...'; ?>
              </div>
              <div class="panel-body">
                <img class="img-responsive mainimage center-block" src="<?php echo $item['item_image']; ?>"> </br>

                <span><b>Цена Amazon:</b> <?php echo $item['price'];?>$</span></br>
                <span class="ok" title="new"><b>Доставка:</b></span> <?php echo item_shipping_els_self($item); ?>$-<?php echo item_shipping_gidex_card($item); ?>$ </br>

            <a href="<?php echo base_url('item/watch/' . $item['id']); ?>">Подробнее</a> 
              </div>
            </div>
        </div>   

        <?php endforeach; ?>
           
    </div>

