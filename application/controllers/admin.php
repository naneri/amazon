<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
    
        public function __construct() {
             parent::__construct();
             $this->load->helper('form');
             $this->load->helper('url');
             $this->load->helper('date');
             $this->load->library('AmazonECS');
             $this->load->library('upload');
             $this->load->helper('item');
             $this->load->library('ion_auth');
             $this->load->model('item_model');
             
         }
         
         public function index(){
             if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
                
           $this->load->library('pagination');

            $paging_url = base_url('admin/index');
            // Configuring and initializing pagination 
            $config['base_url'] = $paging_url;
            $config['total_rows'] = $this->db->count_all('items');
            $config['per_page'] =8;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config); 
            $data['links'] = $this->pagination->create_links();
            
            // Setting the info to get from the database
            $data['per_page'] = $config['per_page']; 
            $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            
            // Getting the records from databse
            $data['items'] = $this->item_model->get_all($data);
            
                    // Rendering the view
            $this->load->view('admin/template/header.php');
            $this->load->view('admin/index.php',$data);
            $this->load->view('admin/template/footer.php');
	}
        
        
        /**
         * Add item page
         */
        public function add(){
            if(!$this->ion_auth->is_admin()){
                redirect('auth/login');
            }
                $this->load->view('admin/template/header.php');
		$this->load->view('admin/add.php');
                $this->load->view('admin/template/footer.php');
	}
        
        /*
         * Process post from Add item page and adding it to the database
         */
        public function create()
	{
            if(!$this->ion_auth->is_admin()){
                redirect('auth/login');
            }
            $id = $this->input->post('asin');
            $data = $this->item_model->get_from_amazon($id);
            
            //preparing the flash messege
            $flash = substr($data['title'], 0 ,30) . '... ' . 'успешно добавлен';
            $this->session->set_flashdata('item', $flash);
            
            // Checking if it is a cellphone
            $data['if_cell_phone'] = $this->input->post('ifcellphone');
            
            // Inserting all the info into the database
            if($this->item_model->set_item($data)){
                $iteminfo['status'] = 'success';
            } 
            
            // Loading the view
             redirect('admin/add');
                
	}
        
        /**
         * Edit item controller
         */
        public function edit(){
            
            //Checks if the user is logged in
            if(!$this->ion_auth->is_admin()){
                redirect('auth/login');
            }
            
            //Gets the item id from the URL
            $id = $this->uri->segment(3);
            
            //Gets the item info from the database
            $data['item'] = $this->item_model->get_item($id);
            
            //loads the view
            $this->load->view('admin/template/header.php');
            $this->load->view('admin/edit.php', $data);
            $this->load->view('admin/template/footer.php');
	}
        
        /**
         * Updates the edited item
         */
        public function update(){
            
            if(!$this->ion_auth->is_admin()){
                redirect('auth/login');
            }
            
            $this->item_model->update_item();
            redirect('admin/index');
        }
        
        
        public function delete(){
            if(!$this->ion_auth->is_admin()){
                redirect('auth/login');
            }
                $id = $this->uri->segment(3);
                $this->item_model->delete_item($id);
                $flash = 'Item successfully deleted';
                $this->session->set_flashdata('item', $flash);
                redirect('admin/index');
	}
        
        
}