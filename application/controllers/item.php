<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller {
    
    public function __construct() {
         parent::__construct();
         $this->load->helper('form');
         $this->load->helper('url');
         $this->load->helper('date');
         $this->load->library('AmazonECS');
         $this->load->library('upload');
         $this->load->helper('item');

         $this->load->model('item_model');
     
     }

    /*
     * Showing the info of the first 8 items
     */
    public function index() {
        // Loading pagination
        
    $this->load->library('pagination');

    $paging_url = base_url('item/index');
    // Configuring and initializing pagination 
    $config['base_url'] = $paging_url;
    $config['total_rows'] = $this->db->count_all('items');
    $config['per_page'] =8;
    $config["uri_segment"] = 3;
    $this->pagination->initialize($config); 
    $data['links'] = $this->pagination->create_links();

    // Setting the info to get from the database
    $data['per_page'] = $config['per_page']; 
    $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

    // Getting the records from databse
    $data['items'] = $this->item_model->get_all($data);

    // Rendering the view
    $this->load->view('template/header.php');
    $this->load->view('item/index.php',$data);
    $this->load->view('template/footer.php');
    }

    /*
     * Watch a single item info
     */
    public function watch($id){

    // Getting the game['id'] from the url
    $data['item'] = $this->item_model->get_item($id);
    $data['title'] = 'Заказать ' . $data['item']['title'] . ' в Бишкеке';
    
    // Rendering the page
    $this->load->view('template/header', $data);
    $this->load->view('item/watch',$data);
    $this->load->view('template/footer');
    }

    /**
     * Loading form to check item by users
     */
    public function sample(){

             $this->load->view('template/header.php');
             $this->load->view('item/sample.php');
             $this->load->view('template/footer.php');
     }

    /**
     * Processing post with provided asin and prices to get item info
     * and calculate its' shipping to kyrgyzstan
     */
    public function getsample()
    {
        // Getting the $id from the Post request
        $id = $this->input->post('asin');

        // Retreiving amazon info for the provided $id
        $data = $this->item_model->get_from_amazon($id);
        
        // Checking if it is a cellphone
        $data['if_cell_phone'] = $this->input->post('ifcellphone');

        // Loading the view
        $this->load->view('template/header.php');
        $this->load->view('item/info',$data);
        $this->load->view('template/footer.php');

    }

    /**
     * The page "About us"
     */
    public function about()
    {
        $this->load->view('template/header.php');
        $this->load->view('item/about');
        $this->load->view('template/footer.php');
    }       
        
    
    public function priceupdate()
    {
        $id = $this->uri->segment(3);
        $this->item_model->priceupdate($id);
        
        $flash = 'Уведомление об изменении цены отправлено администратору.';
        $this->session->set_flashdata('item', $flash);
        redirect($this->session->userdata('previous_page'));
    } 
    
}