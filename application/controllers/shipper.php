<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipper extends CI_Controller {
    
    public function __construct() {
        
             parent::__construct();
             $this->load->helper('url');
             $this->load->helper('item');
             
             $this->load->model('item_model');
             $this->load->model('shipper_model');
             
         }
	
    /**
     * Page with instructions of how to order via Shipper card
     */
    public function order_card()
    {
        $product_id = $this->uri->segment(4, 0);
        $shipper_name = $this->uri->segment(3, 0);
        $data['item'] = $this->item_model->get_item($product_id);
        $data['shipper'] = $this->shipper_model->get_shipper($shipper_name);
        $this->session->set_userdata('previous_page', uri_string()); 
        // Loading the view
        $this->load->view('template/header.php');
        $this->load->view('shipper/order_card',$data);
        $this->load->view('template/footer.php');
    }
    
    /**
     * Page with instructions of how to order goods yourself
     */
    public function order_self()
    {
        $product_id = $this->uri->segment(4, 0);
        $shipper_name = $this->uri->segment(3, 0);
        $data['item'] = $this->item_model->get_item($product_id);
        $data['shipper'] = $this->shipper_model->get_shipper($shipper_name);
        $this->session->set_userdata('previous_page', uri_string()); 
        // Loading the view
        $this->load->view('template/header.php');
        $this->load->view('shipper/order_self',$data);
        $this->load->view('template/footer.php');
    } 
}