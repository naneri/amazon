<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * The function calculates the shipping price via Gidex, using gidex card
 */
if ( ! function_exists('item_shipping_gidex_card'))
{
        function item_shipping_gidex_card($item)
	{
                if ($item['if_cell_phone'] == 1){
                    return ceil($item['price']*0.1)+15;
                }
                
                return ceil($item['price']*0.1 + ceil($item['max_weight']/100)*15);
	}
}

/**
 * The function calculates the shipping price via Gidex, using customers own card
 */
if ( ! function_exists('item_shipping_gidex_self'))
{
        function item_shipping_gidex_self($item)
	{
                if ($item['if_cell_phone'] == 1){
                    return 25;
                }
                
                return ceil($item['max_weight']/100)*15+10;
	}
}

/**
 * The function calculates the shipping via ELS using ELS card
 */
if ( ! function_exists('item_shipping_els_card'))
{
        function item_shipping_els_card($item)
	{
                if ($item['if_cell_phone'] == 1){
                    return ceil($item['price']*0.07)+10;
                }
                
                return ceil($item['price']*0.07 + ceil($item['max_weight']/100)*10);
	}
}

/**
 * The function calculates the shipping via ELS using customers  own card
 */
if ( ! function_exists('item_shipping_els_self'))
{
        function item_shipping_els_self($item)
	{                
                if ($item['if_cell_phone'] == 1){
                    return 10;
                }
                
                return ceil($item['max_weight']/100)*10;
	}
}
