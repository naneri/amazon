<?php

class Item_model extends CI_Model{

    /**
     * Gets items information
     * 
     * @param   type array  $data   number of the page and limit per page
     * @return  type array      
     */
    public function get_all($data = FALSE) {
        
        // if $data['limit'] is not given then all records are retrieven
        $this->db->limit($data['per_page'], $data['page']);
        $this->db->order_by("id","desc");
        $query = $this->db->get('items');
        return $query->result_array();
    }

    /*
     * Gets a single game
     */
    public function get_item($id){
        $query = $this->db->get_where('items', array('id' => $id));
        return $query->row_array();
    }
    
    /**
     * Sets item info
     * 
     * @param   type    array    $data data of the item
     * @return  type
     */
    public function set_item($data = false) 
    {
       $this->db->insert('items', $data);
       return $this->db->insert_id();
    }
    
    /**
     * Deletes item according to provided id
     * 
     * @param type $id
     * @return type
     */
    public function delete_item($id) 
    {
       $this->db->delete('items', array('id' => $id));
       return $this->db->insert_id();
    }
    
    public function update_item() 
    {
        $id = $this->input->post('id');
        $description = $this->input->post('description');
        $price = $this->input->post('price');
        
        $data = array(
               'description' => $description,
               'price' => $price,
               'updated' => 0
            );

        $this->db->where('id', $id);
        $this->db->update('items', $data); 
    }
    
    /**
     * Gets item info from amazon according to provided ASIN (amazon product id number)
     * 
     * @param   type $id - which is the ASIN number
     * 
     * @return  array of the product info. It includes ASIN, product amazon URL, package size, package weight, image URL, etc.
     */
    public function get_from_amazon($id)
    {
        // Instantiating a new AmazonECS object
        $amazonEcs = new AmazonECS();
        
        // Fetching the Item data from amazon and reencoding from object to an array
        $response = $amazonEcs->responseGroup('Medium')->lookup($id);
        $json = json_encode($response);
        $info = json_decode($json,true);
        
        // Assigning a ASIN number to a variable
        $data['asin'] = $id;
        
        // Assigning url with description to a varibale
        $data['url'] = $info['Items']['Item']["DetailPageURL"];
            
        // Assigning url with item image to a varibale
        $data['item_image'] = $info['Items']['Item']["LargeImage"]['URL'];
        
        // Creating a new variable to work with only for item attributes
        $attr = $info['Items']['Item']["ItemAttributes"];
            
        // Creating item title using its' Label and Model
        $data['title']  = $attr['Title'];

        // Assigning item price to a varibale
        $data['price'] = intval($this->input->post('price'));

        // Assigning item package weight to a varibale
        $data['weight'] = $attr["PackageDimensions"]["Weight"]["_"];

        // Assigning item package dimensions to a varibale
        $data['dimension_weight'] = round($attr["PackageDimensions"]['Height']['_']*$attr["PackageDimensions"]['Length']['_']*$attr["PackageDimensions"]['Width']['_']);
        $data['height'] = ceil($attr["PackageDimensions"]['Height']['_']*2.54/100);
        $data['length'] = ceil($attr["PackageDimensions"]['Length']['_']*2.54/100);
        $data['width'] = ceil($attr["PackageDimensions"]['Width']['_']*2.54/100);

        // Calculating max weight (weight vs volume weight)
        $a = $data['weight']*0.0045;
        $b = $data['dimension_weight']*16.77*0.2/1000000000;
        $c = max($a,$b)*100;

        $data['max_weight'] = $c;
        
        
        return $data;
    }
    
    public function priceupdate($id){
        
        $data = array(
                'updated' => 1
            );

        $this->db->where('id', $id);
        $this->db->update('items', $data); 
        
    }
    
}