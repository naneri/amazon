<?php

class Shipper_model extends CI_Model{
    
    public function __construct() {
        $this->load->database();
    }
    
    public function get_shipper($name){
        $query = $this->db->get_where('shipper', array('name' => $name));
        return $query->row_array();
    }
    
}